package com.junitdemo.junits.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FunDto {
    private Integer id;
    private String funText1;
    private String funText2;
}
