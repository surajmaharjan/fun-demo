package com.junitdemo.junits.dto.apiresponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FunResponseDto {
    private Boolean status;
    private String message;
    private Object data;
}
