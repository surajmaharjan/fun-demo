package com.junitdemo.junits.rapipay.mq.config;

import com.junitdemo.junits.rapipay.mq.constants.MqConstants;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

    @Bean
    DirectExchange deadLetterExchange() {
        return new DirectExchange(MqConstants.DL_EXCHANGE_NAME);
    }

    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(MqConstants.DL_QUEUE_NAME).build();
    }

    @Bean
    Binding DLQBinding() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(MqConstants.DL_ROUTING_KEY);
    }

    @Bean
    Queue mailQueue() {
        return QueueBuilder.durable(MqConstants.QUEUE_NAME).withArgument("x-dead-letter-exchange", MqConstants.DL_EXCHANGE_NAME)
                .withArgument("x-dead-letter-routing-key", MqConstants.DL_ROUTING_KEY).build();
    }


    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(MqConstants.EXCHANGE_NAME);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder
                .bind(mailQueue())
                .to(exchange())
                .with(MqConstants.ROUTING_KEY);
    }
}