package com.junitdemo.junits.rapipay.mq.service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import com.junitdemo.junits.rapipay.mq.entity.MyResponse;

public interface MyResponseService {

    void processMessageFromQueue(Message message) throws JsonProcessingException;

    MyResponse save(MyResponse myResponse);
}
