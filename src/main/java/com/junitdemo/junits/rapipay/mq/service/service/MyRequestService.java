package com.junitdemo.junits.rapipay.mq.service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.junitdemo.junits.rapipay.mq.entity.MyRequest;

import java.util.List;

public interface MyRequestService {
    MyRequest save(MyRequest requestEntity) throws JsonProcessingException;

    List<MyRequest> findAll();
}
