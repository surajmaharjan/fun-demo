package com.junitdemo.junits.rapipay.mq.service.mqservice;

import com.junitdemo.junits.rapipay.mq.constants.MqConstants;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import com.junitdemo.junits.rapipay.mq.service.service.MyResponseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitMQListener {

    private final MyResponseService myResponseService;

    public RabbitMQListener(MyResponseService myResponseService) {
        this.myResponseService = myResponseService;
    }

    @RabbitListener(queues = MqConstants.QUEUE_NAME)
    public void consumeMail(Message message) throws Exception {
        log.info("Message successfully received !!!");
        log.info(message.toString());
        myResponseService.processMessageFromQueue(message);
    }


    @RabbitListener(queues = MqConstants.DL_QUEUE_NAME)
    public void deadLetterListener(Message message) throws Exception {
        log.info("Dead letter message {} ", message.toString());
    }
}
