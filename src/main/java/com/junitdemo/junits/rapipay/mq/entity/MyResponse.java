package com.junitdemo.junits.rapipay.mq.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tbl_response", uniqueConstraints = {

})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MyResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "api_response", columnDefinition = "TEXT")
    private String apiResponse;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", foreignKey = @ForeignKey(name = "FK_RESPONSE_ID"))
    private MyRequest myRequest;
}
