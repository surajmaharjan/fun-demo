package com.junitdemo.junits.rapipay.mq.service.mqservice;

import com.junitdemo.junits.rapipay.mq.constants.MqConstants;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RabbitMQProducer {


    private final RabbitTemplate rabbitTemplate;

    public RabbitMQProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void produceMessage(Message message) {
        log.info("MESSAGE READY TO BE SENT {}", message.getMId());
        rabbitTemplate.convertAndSend(MqConstants.EXCHANGE_NAME, MqConstants.ROUTING_KEY, message);
        log.info("MESSAGE SENT {}", message.getMId());
    }
}