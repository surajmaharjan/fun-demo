package com.junitdemo.junits.rapipay.mq.repo;

import com.junitdemo.junits.rapipay.mq.entity.MyResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyResponseRepository extends JpaRepository<MyResponse, Integer> {
}
