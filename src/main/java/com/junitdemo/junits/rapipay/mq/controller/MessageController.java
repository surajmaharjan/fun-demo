package com.junitdemo.junits.rapipay.mq.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.junitdemo.junits.controllers.base.BaseController;
import com.junitdemo.junits.dto.apiresponse.FunResponseDto;
import com.junitdemo.junits.rapipay.mq.entity.MyRequest;
import com.junitdemo.junits.rapipay.mq.service.service.MyRequestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessageController extends BaseController {

    private final MyRequestService myRequestService;

    public MessageController(MyRequestService myRequestService) {
        this.myRequestService = myRequestService;
    }


    @PostMapping
    public ResponseEntity<FunResponseDto> postMessage(@RequestBody MyRequest myRequest) throws JsonProcessingException {
        myRequest = myRequestService.save(myRequest);
        return new ResponseEntity<>(successResponse("Message Saved and Sent", myRequest), HttpStatus.OK);
    }

}
