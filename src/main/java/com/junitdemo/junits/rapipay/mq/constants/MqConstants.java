package com.junitdemo.junits.rapipay.mq.constants;

public class MqConstants {
    public static final String QUEUE_NAME = "custom.queue";
    public static final String EXCHANGE_NAME = "custom.exchange";
    public static final String ROUTING_KEY = "custom.key";

    public static final String DL_QUEUE_NAME = "deadLetterQueue";
    public static final String DL_EXCHANGE_NAME = "deadLetterExchange";
    public static final String DL_ROUTING_KEY = "deadLetter";
}
