package com.junitdemo.junits.service.impl;

import com.junitdemo.junits.dto.FunDto;
import com.junitdemo.junits.entity.Fun;
import com.junitdemo.junits.repo.FunRepo;
import com.junitdemo.junits.service.FunService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FunServiceImpl implements FunService {

    private final FunRepo funRepo;

    public FunServiceImpl(FunRepo funRepo) {
        this.funRepo = funRepo;
    }

    @Override
    public FunDto createFun(FunDto dto) throws NullPointerException {
        Fun entity = Fun.builder()
                .id(dto.getId())
                .funText1(dto.getFunText1())
                .funText2(dto.getFunText2())
                .build();
        entity = funRepo.save(entity);
        return FunDto.builder()
                .id(entity.getId())
                .funText1(entity.getFunText1())
                .funText2(entity.getFunText2())
                .build();
    }

    @Override
    public List<FunDto> findAll() {
        return funRepo.findAll().stream().map(fun ->
                FunDto.builder()
                        .id(fun.getId())
                        .funText1(fun.getFunText1())
                        .funText2(fun.getFunText2())
                        .build()
        ).collect(Collectors.toList());
    }

    @Override
    public FunDto findById(Integer id) {
        Optional<Fun> optionalFun = funRepo.findById(id);
        if (optionalFun.isPresent()) {
            Fun fun = optionalFun.get();
            return FunDto.builder()
                    .id(fun.getId())
                    .funText1(fun.getFunText1())
                    .funText2(fun.getFunText2())
                    .build();
        } else {
            return null;
        }
    }
}
