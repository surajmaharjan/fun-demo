package com.junitdemo.junits.service;

import com.junitdemo.junits.dto.FunDto;

import java.util.List;

public interface FunService {

    FunDto createFun(FunDto dto);

    List<FunDto> findAll();

    FunDto findById(Integer id);

}
