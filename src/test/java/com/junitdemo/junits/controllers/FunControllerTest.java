package com.junitdemo.junits.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.junitdemo.junits.dto.FunDto;
import com.junitdemo.junits.dto.apiresponse.FunResponseDto;
import com.junitdemo.junits.service.FunService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//L!m3l1g#t2020

@Slf4j
@ExtendWith(MockitoExtension.class)
@WebMvcTest(FunController.class)
//@ActiveProfiles("test")
public class FunControllerTest {

    /**
     * @MockMVCTest : annotation is used for Spring MVC tests. It disables full auto-configuration
     * and instead apply only configuration relevant to MVC tests.
     */

    /**
     * @MockMvc : is a class part of Spring MVC Test which help you to test controllers
     * explicitly starting a Servlet container.
     * and then, we create dummy data on funDtoList.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Actually we are testing controller. So controller is dependent on Service
     * So MockBean mocks the service for testing purpose
     */
    @MockBean
    private FunService funService;

    private ObjectMapper objectMapper = new ObjectMapper();

    private List<FunDto> funDtoList;

    private FunDto funDto;

    @BeforeEach
    void setUpObject() {
        this.funDto = new FunDto(1, "text1", "mail1@gmail.com");
        this.funDtoList = new ArrayList<>();
        this.funDtoList.add(new FunDto(1, "text1", "mail1@gmail.com"));
        this.funDtoList.add(new FunDto(2, "text2", "mail2@gmail.com"));
        this.funDtoList.add(new FunDto(3, "text3", "mail3@gmail.com"));
    }

    @Test
    void createFunTest() throws Exception {

        Mockito.when(funService.createFun(funDto)).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        RequestBuilder request = MockMvcRequestBuilders
                .post("/fun")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(funDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE);


        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isCreated()).andReturn();

        System.out.println(mvcResult);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void findAllFun() throws Exception {
        Mockito.when(funService.findAll()).thenReturn(funDtoList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/fun")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", Is.is(true)))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        FunResponseDto funResponseDto = objectMapper.readValue(response, FunResponseDto.class);
        Assertions.assertTrue(funResponseDto.getStatus());
        Assertions.assertTrue(funResponseDto.getData() != null);
    }

    @Test
    public void findByIdReturnsValidData() throws Exception {
        Mockito.when(funService.findById(10)).thenReturn(new FunDto(1, "funText", "suraj@gmail.com"));

        RequestBuilder request = MockMvcRequestBuilders
                .get("/fun/10")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        FunResponseDto funResponseDto = objectMapper.readValue(response, FunResponseDto.class);
        Assertions.assertTrue(funResponseDto.getStatus());
        Assertions.assertTrue(funResponseDto.getData() != null);
    }


    @Test
    public void findByIdDataNotFoundCase() throws Exception {
        Mockito.when(funService.findById(15)).thenReturn(null);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/fun/15")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", Is.is(false)))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        FunResponseDto responseDto = objectMapper.readValue(response, FunResponseDto.class);

        Assertions.assertTrue(responseDto.getData() == null);
    }

    @Test
    public void checkForValidFunDtoList() throws Exception {
        Mockito.when(funService.findAll()).thenReturn(funDtoList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/fun/list")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(request)
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.status", Is.is(true)))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        List<FunDto> funDtoList = objectMapper.readValue(response, new TypeReference<List<FunDto>>() {
        });

        Assertions.assertTrue(funDtoList.size() == 3);
    }

    /**
     * This will execute after all the test executes
     * If you have to destroy any predefined things you can destroy here
     */
    @AfterAll
    public static void displayTestCompletionMessage() {
        log.info("Test completed for FunController");
    }
}